package itemmaker;

import com.jfoenix.controls.*;
import itemmaker.model.Elements;
import itemmaker.item.Item;
import itemmaker.model.Model;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.util.Builder;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller {


    // PRIVATE

    private Elements elements;
    private Item item;
    private Model model;
    private Parent root;
    private List<Elements> equipList = null;
    private List<Elements> trinketList = null;
    private List<Elements> weaponList = null;
    private List<Elements> mobList = null;
    private boolean firstSession;

    private ObservableList obsList = FXCollections.observableArrayList();

    public void setRoot(Parent root) {
        this.root = root;
    }

    public void setModel(Model model) {
        this.model = model;
    }
    /**
     *
     * @param field Concerned field
     * @return parseDouble(field);
     */
    private double getValueFrom(JFXTextField field) {
        return Double.parseDouble(field.getText());
    }

    private void resetAllFIeld() {
        fieldAccuracy.setText("");
        fieldAgility.setText("");
        fieldBleedingStacks.setText("");
        fieldCriticalChance.setText("");
        fieldDextrerity.setText("");
        fieldEndurance.setText("");
        fieldEvasion.setText("");
        fieldIntelligence.setText("");
        fieldLevel.setText("");
        fieldLuck.setText("");
        fieldMgDamage.setText("");
        fieldMgDefence.setText("");
        fieldNameOfObject.setText("");
        fieldPerception.setText("");
        fieldPhDamage.setText("");
        fieldPhDefence.setText("");
        fieldPureDamage.setText("");
        fieldStunChance.setText("");
        fieldVitality.setText("");
        fieldWisdom.setText("");
        fieldSpeed.setText("");
        fieldStrenght.setText("");
        textAreaDescription.setText("");
    }

    private void createTooltip(String text) {
        Tooltip tt = new Tooltip(text);
        tt.show(root.getScene().getWindow(), root.getScene().getWindow().getX() + (root.getScene().getWindow().getWidth()/2), root.getScene().getWindow().getY() + (root.getScene().getWindow().getHeight()/2));
        tt.setFont(new Font("Nirmala UI", 18));
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(3000), e -> {
            tt.hide();
        }));
        timeline.setCycleCount(1);
        timeline.play();
    }

    // PRIVATE


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="list"
    private JFXListView<Item> list; // Value injected by FXMLLoader

    @FXML // fx:id="fieldNameOfObject"
    private JFXTextField fieldNameOfObject; // Value injected by FXMLLoader

    @FXML // fx:id="fieldVitality"
    private JFXTextField fieldVitality; // Value injected by FXMLLoader

    @FXML // fx:id="isEquip"
    private JFXRadioButton isEquip; // Value injected by FXMLLoader

    @FXML // fx:id="choiceElementOfEquip"
    private ChoiceBox<Elements> choiceElementOfEquip; // Value injected by FXMLLoader

    @FXML // fx:id="isTrinket"
    private JFXRadioButton isTrinket; // Value injected by FXMLLoader

    @FXML // fx:id="choiceElementOfTrinket"
    private ChoiceBox<Elements> choiceElementOfTrinket; // Value injected by FXMLLoader

    @FXML // fx:id="isWeapon"
    private JFXRadioButton isWeapon; // Value injected by FXMLLoader

    @FXML // fx:id="choiceElementOfWeapon"
    private ChoiceBox<Elements> choiceElementOfWeapon; // Value injected by FXMLLoader

    @FXML // fx:id="isMob"
    private JFXRadioButton isMob; // Value injected by FXMLLoader

    @FXML // fx:id="choiceElementOfMob"
    private ChoiceBox<Elements> choiceElementOfMob; // Value injected by FXMLLoader

    @FXML // fx:id="fieldStrenght"
    private JFXTextField fieldStrenght; // Value injected by FXMLLoader

    @FXML // fx:id="fieldAgility"
    private JFXTextField fieldAgility; // Value injected by FXMLLoader

    @FXML // fx:id="fieldDextrerity"
    private JFXTextField fieldDextrerity; // Value injected by FXMLLoader

    @FXML // fx:id="fieldEndurance"
    private JFXTextField fieldEndurance; // Value injected by FXMLLoader

    @FXML // fx:id="fieldIntelligence"
    private JFXTextField fieldIntelligence; // Value injected by FXMLLoader

    @FXML // fx:id="fieldWisdom"
    private JFXTextField fieldWisdom; // Value injected by FXMLLoader

    @FXML // fx:id="fieldPerception"
    private JFXTextField fieldPerception; // Value injected by FXMLLoader

    @FXML // fx:id="fieldLevel"
    private JFXTextField fieldLevel; // Value injected by FXMLLoader

    @FXML // fx:id="choiceRarity"
    private ChoiceBox<String> choiceRarity; // Value injected by FXMLLoader

    @FXML // fx:id="textAreaDescription"
    private JFXTextArea textAreaDescription; // Value injected by FXMLLoader

    @FXML // fx:id="fieldPhDamage"
    private JFXTextField fieldPhDamage; // Value injected by FXMLLoader

    @FXML // fx:id="fieldMgDamage"
    private JFXTextField fieldMgDamage; // Value injected by FXMLLoader

    @FXML // fx:id="fieldSpeed"
    private JFXTextField fieldSpeed; // Value injected by FXMLLoader

    @FXML // fx:id="fieldAccuracy"
    private JFXTextField fieldAccuracy; // Value injected by FXMLLoader

    @FXML // fx:id="fieldEvasion"
    private JFXTextField fieldEvasion; // Value injected by FXMLLoader

    @FXML // fx:id="fieldCriticalChance"
    private JFXTextField fieldCriticalChance; // Value injected by FXMLLoader

    @FXML // fx:id="fieldStunChance"
    private JFXTextField fieldStunChance; // Value injected by FXMLLoader

    @FXML // fx:id="fieldBleedingStacks"
    private JFXTextField fieldBleedingStacks; // Value injected by FXMLLoader

    @FXML // fx:id="fieldPureDamage"
    private JFXTextField fieldPureDamage; // Value injected by FXMLLoader

    @FXML // fx:id="fieldPhDefence"
    private JFXTextField fieldPhDefence; // Value injected by FXMLLoader

    @FXML // fx:id="fieldMgDefence"
    private JFXTextField fieldMgDefence; // Value injected by FXMLLoader

    @FXML // fx:id="bntAddToList"
    private JFXButton bntAddToList; // Value injected by FXMLLoader

    @FXML // fx:id="btnRemoveFromList"
    private JFXButton btnRemoveFromList; // Value injected by FXMLLoader

    @FXML // fx:id="btnDeleteList"
    private JFXButton btnDeleteList; // Value injected by FXMLLoader

    @FXML // fx:id="btnSave"
    private JFXButton btnSave; // Value injected by FXMLLoader

    @FXML // fx:id="fieldNameOfSave"
    private JFXTextField fieldNameOfSave; // Value injected by FXMLLoader

    @FXML // fx:id="btnLoad"
    private JFXButton btnLoad; // Value injected by FXMLLoader

    @FXML // fx:id="fieldLuck"
    private JFXTextField fieldLuck; // Value injected by FXMLLoader

    @FXML // fx:id="btnAlteration"
    private JFXButton btnAlteration; // Value injected by FXMLLoader


    @FXML
    void doAddToList(MouseEvent event) {
        item = null;
        if (isEquip.isSelected()) {
            item = new Item(choiceElementOfEquip.getSelectionModel().getSelectedItem().getSlotInventory(), fieldNameOfObject.getText(),
                    (int) getValueFrom(fieldLevel), choiceRarity.getValue(), textAreaDescription.getText(),
                    getValueFrom(fieldVitality), getValueFrom(fieldStrenght), getValueFrom(fieldAgility),
                    getValueFrom(fieldDextrerity), getValueFrom(fieldEndurance), getValueFrom(fieldIntelligence),
                    getValueFrom(fieldWisdom), getValueFrom(fieldPerception), getValueFrom(fieldPhDamage),
                    getValueFrom(fieldMgDamage), getValueFrom(fieldSpeed), getValueFrom(fieldStunChance),
                    getValueFrom(fieldBleedingStacks), getValueFrom(fieldAccuracy), getValueFrom(fieldEvasion),
                    getValueFrom(fieldCriticalChance), getValueFrom(fieldPhDefence), getValueFrom(fieldMgDefence),
                    getValueFrom(fieldPureDamage), getValueFrom(fieldLuck));

            if (!obsList.contains(item)) {
                obsList.add(item);
                list.setItems(obsList);
                resetAllFIeld();
            } else {
                createTooltip("Object already inserted");
            }
        } else if (isTrinket.isSelected()) {
            item = new Item(choiceElementOfTrinket.getSelectionModel().getSelectedItem().getSlotInventory(), fieldNameOfObject.getText(),
                    (int) getValueFrom(fieldLevel), choiceRarity.getValue(), textAreaDescription.getText(),
                    getValueFrom(fieldVitality), getValueFrom(fieldStrenght), getValueFrom(fieldAgility),
                    getValueFrom(fieldDextrerity), getValueFrom(fieldEndurance), getValueFrom(fieldIntelligence),
                    getValueFrom(fieldWisdom), getValueFrom(fieldPerception), getValueFrom(fieldPhDamage),
                    getValueFrom(fieldMgDamage), getValueFrom(fieldSpeed), getValueFrom(fieldStunChance),
                    getValueFrom(fieldBleedingStacks), getValueFrom(fieldAccuracy), getValueFrom(fieldEvasion),
                    getValueFrom(fieldCriticalChance), getValueFrom(fieldPhDefence), getValueFrom(fieldMgDefence),
                    getValueFrom(fieldPureDamage), getValueFrom(fieldLuck));

            if (!obsList.contains(item)) {
                obsList.add(item);
                list.setItems(obsList);
                resetAllFIeld();
            } else {
                createTooltip("Object already inserted");
            }
        } else if (isWeapon.isSelected()) {
            item = new Item(choiceElementOfWeapon.getSelectionModel().getSelectedItem().getSlotInventory(), fieldNameOfObject.getText(),
                    (int) getValueFrom(fieldLevel), choiceRarity.getValue(), textAreaDescription.getText(),
                    getValueFrom(fieldVitality), getValueFrom(fieldStrenght), getValueFrom(fieldAgility),
                    getValueFrom(fieldDextrerity), getValueFrom(fieldEndurance), getValueFrom(fieldIntelligence),
                    getValueFrom(fieldWisdom), getValueFrom(fieldPerception), getValueFrom(fieldPhDamage),
                    getValueFrom(fieldMgDamage), getValueFrom(fieldSpeed), getValueFrom(fieldStunChance),
                    getValueFrom(fieldBleedingStacks), getValueFrom(fieldAccuracy), getValueFrom(fieldEvasion),
                    getValueFrom(fieldCriticalChance), getValueFrom(fieldPhDefence), getValueFrom(fieldMgDefence),
                    getValueFrom(fieldPureDamage), getValueFrom(fieldLuck));

            if (!obsList.contains(item)) {
                obsList.add(item);
                list.setItems(obsList);
                resetAllFIeld();
            } else {
                createTooltip("Object already inserted");
            }
        }

    }

    @FXML
    void doRemoveFromList(MouseEvent event) {
        if (list.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Caution!");
            alert.setContentText("Be careful to delete an item from the list, confirm it?");
            alert.setHeaderText(null);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get().equals(ButtonType.OK)) {
                list.getItems().remove(list.getSelectionModel().getSelectedItem());
                obsList.remove(list.getSelectionModel().getSelectedItem());
            }
        } else {
            createTooltip("First select something");
        }
    }

    @FXML
    void doDeleteList(MouseEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Caution!");
        alert.setContentText("Warning you are about to clear all the list Are you sure?");
        alert.setHeaderText(null);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get().equals(ButtonType.OK)) {
            list.getItems().clear();
            obsList.clear();
        }
    }

    @FXML
    void doInspectList(MouseEvent event) {
        ContextMenu cm = new ContextMenu();
        MenuItem menuItem = new MenuItem();
        menuItem.setText("Alter");
        cm.getItems().add(menuItem);

        if (event.getButton().equals(MouseButton.SECONDARY) && !cm.isShowing()) {
            resetAllFIeld();
            cm.show(list, event.getScreenX(), event.getScreenY());
            menuItem.setOnAction(e -> {
                fieldAccuracy.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getAccuracy()));
                fieldAgility.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getAgility()));
                fieldBleedingStacks.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getBleedingStacks()));
                fieldCriticalChance.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getCriticalStrikeChance()));
                fieldDextrerity.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getDextrerity()));
                fieldEndurance.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getEndurance()));
                fieldEvasion.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getEvasion()));
                fieldIntelligence.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getIntelligence()));
                fieldLevel.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getLevel()));
                fieldLuck.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getLuck()));
                fieldMgDamage.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getMagicDamage()));
                fieldMgDefence.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getMagicDefence()));
                fieldNameOfObject.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getName()));
                fieldPerception.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getPerception()));
                fieldPhDamage.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getPhisicalDamage()));
                fieldPhDefence.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getPhisicalDefence()));
                fieldPureDamage.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getPureDamageEffectiveness()));
                fieldStunChance.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getStunChance()));
                fieldVitality.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getVitality()));
                fieldWisdom.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getWisdom()));
                fieldSpeed.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getSpeed()));
                fieldStrenght.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getStrenght()));
                textAreaDescription.setText(String.valueOf(list.getSelectionModel().getSelectedItem().getDescription()));
            });
            Timeline timeline = new Timeline(new KeyFrame(Duration.millis(5000), e -> {
                cm.hide();
            }));
            timeline.setCycleCount(1);
            timeline.play();
        }
    }

    @FXML
    void doAlterationItemSelected(MouseEvent event) {
        item = null;
        item = new Item(choiceElementOfEquip.getSelectionModel().getSelectedItem().getSlotInventory(), fieldNameOfObject.getText(),
                (int) getValueFrom(fieldLevel), choiceRarity.getValue(), textAreaDescription.getText(),
                getValueFrom(fieldVitality), getValueFrom(fieldStrenght), getValueFrom(fieldAgility),
                getValueFrom(fieldDextrerity), getValueFrom(fieldEndurance), getValueFrom(fieldIntelligence),
                getValueFrom(fieldWisdom), getValueFrom(fieldPerception), getValueFrom(fieldPhDamage),
                getValueFrom(fieldMgDamage), getValueFrom(fieldSpeed), getValueFrom(fieldStunChance),
                getValueFrom(fieldBleedingStacks), getValueFrom(fieldAccuracy), getValueFrom(fieldEvasion),
                getValueFrom(fieldCriticalChance), getValueFrom(fieldPhDefence), getValueFrom(fieldMgDefence),
                getValueFrom(fieldPureDamage), getValueFrom(fieldLuck));
        if (!obsList.contains(item)) {
            list.getItems().remove(list.getSelectionModel().getSelectedItem());
            obsList.remove(list.getSelectionModel().getSelectedItem());
            list.getItems().add(item);
            obsList.add(item);
        } else {
            createTooltip("You can not create an existing object");
        }

    }

    @FXML
    void doEquip(MouseEvent event) {
        if (!firstSession) {
            createTooltip("Be careful about changing the object setting,\n" +
                    "it is highly advised not to end it in the same session.");
        } else {
            firstSession = false;
        }
        if (isEquip.isSelected() && equipList == null) {
            elements = null;
            elements = new Elements(true, false, false, false);
            equipList = elements.getList();
            choiceElementOfEquip.getItems().addAll(equipList);
        }
        isTrinket.setSelected(false);
        isWeapon.setSelected(false);
        isMob.setSelected(false);
        choiceElementOfEquip.setDisable(false);
        choiceElementOfTrinket.setDisable(true);
        choiceElementOfWeapon.setDisable(true);
        choiceElementOfMob.setDisable(true);
    }

    @FXML
    void doTrinket(MouseEvent event) {
        if (!firstSession) {
            createTooltip("Be careful about changing the object setting,\n" +
                    "it is highly advised not to end it in the same session.");
        } else {
            firstSession = false;
        }
        if (isTrinket.isSelected() && trinketList == null) {
            elements = null;
            elements = new Elements(false, true, false, false);
            trinketList = elements.getList();
            choiceElementOfTrinket.getItems().addAll(trinketList);
        }
        isEquip.setSelected(false);
        isWeapon.setSelected(false);
        isMob.setSelected(false);
        choiceElementOfEquip.setDisable(true);
        choiceElementOfTrinket.setDisable(false);
        choiceElementOfWeapon.setDisable(true);
        choiceElementOfMob.setDisable(true);
    }

    @FXML
    void doWeapon(MouseEvent event) {
        if (!firstSession) {
            createTooltip("Be careful about changing the object setting,\n" +
                    "it is highly advised not to end it in the same session.");
        } else {
            firstSession = false;
        }
        if (isWeapon.isSelected() && weaponList == null) {
            elements = null;
            elements = new Elements(false, false, true, false);
            weaponList = elements.getList();
            choiceElementOfWeapon.getItems().addAll(weaponList);
        }
        isEquip.setSelected(false);
        isTrinket.setSelected(false);
        isMob.setSelected(false);
        choiceElementOfTrinket.setDisable(true);
        choiceElementOfWeapon.setDisable(false);
        choiceElementOfEquip.setDisable(true);
        choiceElementOfMob.setDisable(true);
    }

    @FXML
    void doMob(MouseEvent event) {
        if (!firstSession) {
            createTooltip("Be careful about changing the object setting,\n" +
                    "it is highly advised not to end it in the same session.");
        } else {
            firstSession = false;
        }
        if (isMob.isSelected() && mobList == null) {
            elements = null;
            elements = new Elements(false, false, false, true);
            mobList = elements.getList();
            choiceElementOfMob.getItems().addAll(mobList);
        }
        isEquip.setSelected(false);
        isTrinket.setSelected(false);
        isWeapon.setSelected(false);
        choiceElementOfTrinket.setDisable(true);
        choiceElementOfWeapon.setDisable(true);
        choiceElementOfEquip.setDisable(true);
        choiceElementOfMob.setDisable(false);
    }

    @FXML
    void doLoadDb(MouseEvent event) {
        FileChooser fc = new FileChooser();
        File file;
        fc.setTitle("Choose a database");
        file = fc.showOpenDialog(root.getScene().getWindow());

        if (file != null) {
            System.out.print(file.getPath());
            obsList.addAll(model.loadDb(file.getPath(), file.getName()));
            list.setItems(obsList);
        }
    }

    @FXML
    void doSaveDb(MouseEvent event) {
        if (fieldNameOfSave != null) {
            model.makeListForDbByListWiev(obsList, fieldNameOfSave.getText());
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert list != null : "fx:id=\"list\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldNameOfObject != null : "fx:id=\"fieldNameOfObject\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldVitality != null : "fx:id=\"fieldVitality\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert isEquip != null : "fx:id=\"isEquip\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert choiceElementOfEquip != null : "fx:id=\"choiceElementOfEquip\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert isTrinket != null : "fx:id=\"isTrinket\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert choiceElementOfTrinket != null : "fx:id=\"choiceElementOfTrinket\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert isWeapon != null : "fx:id=\"isWeapon\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert choiceElementOfWeapon != null : "fx:id=\"choiceElementOfWeapon\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert isMob != null : "fx:id=\"isMob\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert choiceElementOfMob != null : "fx:id=\"choiceElementOfMob\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldStrenght != null : "fx:id=\"fieldStrenght\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldAgility != null : "fx:id=\"fieldAgility\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldDextrerity != null : "fx:id=\"fieldDextrerity\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldEndurance != null : "fx:id=\"fieldEndurance\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldIntelligence != null : "fx:id=\"fieldIntelligence\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldWisdom != null : "fx:id=\"fieldWisdom\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldPerception != null : "fx:id=\"fieldPerception\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldLevel != null : "fx:id=\"fieldLevel\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert choiceRarity != null : "fx:id=\"choiceRarity\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert textAreaDescription != null : "fx:id=\"textAreaDescription\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldPhDamage != null : "fx:id=\"fieldPhDamage\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldMgDamage != null : "fx:id=\"fieldMgDamage\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldSpeed != null : "fx:id=\"fieldSpeed\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldAccuracy != null : "fx:id=\"fieldAccuracy\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldEvasion != null : "fx:id=\"fieldEvasion\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldCriticalChance != null : "fx:id=\"fieldCriticalChance\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldStunChance != null : "fx:id=\"fieldStunChance\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldBleedingStacks != null : "fx:id=\"fieldBleedingStacks\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldPureDamage != null : "fx:id=\"fieldPureDamage\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldPhDefence != null : "fx:id=\"fieldPhDefence\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldMgDefence != null : "fx:id=\"fieldMgDefence\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert bntAddToList != null : "fx:id=\"bntAddToList\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert btnRemoveFromList != null : "fx:id=\"btnRemoveFromList\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert btnDeleteList != null : "fx:id=\"btnDeleteList\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert btnSave != null : "fx:id=\"btnSave\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldNameOfSave != null : "fx:id=\"fieldNameOfSave\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert btnLoad != null : "fx:id=\"btnLoad\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert fieldLuck != null : "fx:id=\"fieldLuck\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";
        assert btnAlteration != null : "fx:id=\"btnAlteration\" was not injected: check your FXML file 'ItemMakerFXML.fxml'.";

        choiceElementOfEquip.setDisable(true);
        choiceElementOfTrinket.setDisable(true);
        choiceElementOfWeapon.setDisable(true);
        choiceElementOfMob.setDisable(true);

        choiceRarity.getItems().add(0, "Common");
        choiceRarity.getItems().add(1, "Uncommon");
        choiceRarity.getItems().add(2, "Rare");
        choiceRarity.getItems().add(3, "Epic");
        choiceRarity.getItems().add(4, "Legendary");
        choiceRarity.getItems().add(5, "Divine");

        firstSession = true;

        list.setBackground(Background.EMPTY);

    }
}

