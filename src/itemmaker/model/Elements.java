package itemmaker.model;

import java.util.ArrayList;
import java.util.List;

public class Elements {

    private boolean isEquip, isTrinket, isWeapon, isMob;
    private List<Elements> list;
    private String name;
    private int slotInventory;

    public Elements(boolean isEquip, boolean isTrinket, boolean isWeapon, boolean isMob) {
        this.isEquip = isEquip;
        this.isTrinket = isTrinket;
        this.isWeapon = isWeapon;
        this.isMob = isMob;
    }

    public Elements(boolean isEquip, boolean isTrinket, boolean isWeapon, boolean isMob, int slotInventory, String name) {
        this.isEquip = isEquip;
        this.isTrinket = isTrinket;
        this.isWeapon = isWeapon;
        this.isMob = isMob;
        this.slotInventory = slotInventory;
        this.name = name;
    }

    public Elements(boolean isEquip, boolean isTrinket, boolean isWeapon, boolean isMob, String name) {
        this.isEquip = isEquip;
        this.isTrinket = isTrinket;
        this.isWeapon = isWeapon;
        this.isMob = isMob;
        this.name = name;
    }


    public List<Elements> getList() {

        if (isEquip) {
            this.list = new ArrayList<Elements>();
            list.add(0, new Elements(true, false, false, false, 0, "HEAD_ARMOR"));
            list.add(1, new Elements(true, false, false, false, 1, "CHEST_ARMOR"));
            list.add(2, new Elements(true, false, false, false, 2, "LEGS_ARMOR"));
            list.add(3, new Elements(true, false, false, false, 3, "FOOTS_ARMOR"));
            list.add(4, new Elements(true, false, false, false, 4, "ARMLEFT_ARMOR"));
            list.add(5, new Elements(true, false, false, false, 5, "ARMRIGHT_ARMOR"));
            return list;
        } else if (isTrinket) {
            this.list = new ArrayList<Elements>();
            list.add(0, new Elements(false, true, false, false, 6,"NECKLACE"));
            list.add(1, new Elements(false, true, false, false, 7,"BRACELET_LEFT"));
            list.add(2, new Elements(false, true, false, false, 8,"BRACELET_RIGHT"));
            return list;
        } else if (isWeapon) {
            this.list = new ArrayList<Elements>();
            list.add(0, new Elements(false, false, true, false, 9, "PRIMARY_WEAPON"));
            list.add(1, new Elements(false, false, true, false, 10, "SECONDARY_WEAPON"));
            return list;
        } else if (isMob) {
            this.list = new ArrayList<Elements>();
            list.add(0, new Elements(false,false,false,true,"MOB"));
            return list;
        }
        return null;
    }

    @Override
    public String toString() {
        if (!isMob) {
            return name + ", slotInventory=" + slotInventory;
        }
        return name;
    }

    public int getSlotInventory() {
        return slotInventory;
    }

}
