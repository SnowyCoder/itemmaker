package itemmaker.model;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import itemmaker.item.Item;
public class Model {

    private List<Item> listOggetti;
    private Item save;
    private DBConnection db;
    
    public Model() {
	this.listOggetti = new ArrayList<Item>();

    }
    
//    public Item trovaSingoloOggetto(String nomeOggetto) {
//        db.createConnection();
//        return db.findOggetto(nomeOggetto);
//    }
//
//    public List<Item> getListAllObj() {
//	db.createConnection();
//	return db.listOggetti();
//    }

	public List<Item> loadDb(String pathSave, String nameDB) {
		db = new DBConnection(nameDB);
		db.createConnection(pathSave);
		return db.getlistOggetti();
	}

    public void addList(Item o) {
        if (!listOggetti.contains(o)) {
            listOggetti.add(o);
        }
    }

    public void makeListForDbByListWiev(List<Item> item, String nameDB) {
    	this.listOggetti = item;
    	db = new DBConnection(nameDB + ".db");
    	db.createDB();
	}

    @Override
    public String toString() {
        return "Model{" + "Oggetto = " + listOggetti.toString() + '}';
    } 
    
    public List<Item> getListOggetti() {
        return listOggetti;
    }
    

    // InnerCLASSE DBConnection -------------------------------------------------------------------------------------

    private class DBConnection {
	
	private String nomeDB;
	private String nameOfTable;
	private List<Item> index = new ArrayList<Item>();
	private Connection con;
	private ResultSet res;
	private PreparedStatement cmd;
	private String pathSaveDefault = System.getProperty("user.dir") + File.separator + "DB_SAVE";

	public DBConnection(String nomeDB) {
		this.nameOfTable = nomeDB.substring(0, (nomeDB.length() -3));
	    this.nomeDB = nomeDB;
		;
	}
	
	/**
	 * se nella cartella user\home non esiste nessun db di nome dal parametro nomeDB, con il nome trade(standard).
	 */
	public void createDB() {
	    boolean exist = new File(pathSaveDefault + File.separator + nomeDB).exists();
	    if (!exist) {
	    	new File(pathSaveDefault).mkdir();
		try {		    
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:" + pathSaveDefault + File.separator + nomeDB);
		    Statement cmd = con.createStatement();
		    cmd.executeUpdate("CREATE TABLE " + nameOfTable + " (\n" +
					"    id                   INTEGER PRIMARY KEY AUTOINCREMENT\n" +
					"                                 NOT NULL\n" +
					"                                 UNIQUE,\n" +
					"    percorsoImg          STRING  NOT NULL,\n" +
					"    name                 STRING  UNIQUE\n" +
					"                                 NOT NULL,\n" +
					"    vitality             DOUBLE  NOT NULL,\n" +
					"    strenght             DOUBLE  NOT NULL,\n" +
					"    agility              DOUBLE  NOT NULL,\n" +
					"    dextrerity           DOUBLE  NOT NULL,\n" +
					"    endurance            DOUBLE  NOT NULL,\n" +
					"    intelligence         DOUBLE  NOT NULL,\n" +
					"    wisdom               DOUBLE  NOT NULL,\n" +
					"    perception           DOUBLE  NOT NULL,\n" +
					"    phisicalDamage       DOUBLE  NOT NULL,\n" +
					"    magicDamage          DOUBLE  NOT NULL,\n" +
					"    speed                DOUBLE  NOT NULL,\n" +
					"    stunChance           DOUBLE  NOT NULL,\n" +
					"    bleedingStacks       DOUBLE  NOT NULL,\n" +
					"    accuracy             DOUBLE  NOT NULL,\n" +
					"    evasion              DOUBLE  NOT NULL,\n" +
					"    criticalStrikeChance DOUBLE  NOT NULL,\n" +
					"    phisicalDefence      DOUBLE  NOT NULL,\n" +
					"    magicDefence         DOUBLE  NOT NULL,\n" +
					"    luck                 DOUBLE  NOT NULL,\n" +
					"    pureDamage           DOUBLE  NOT NULL,\n" +
					"    level                INTEGER NOT NULL,\n" +
					"    rarity               STRING  NOT NULL,\n" +
					"    description          STRING  NOT NULL,\n" +
					"    slotInventory        INTEGER NOT NULL\n" +
					");");
		    setValueOnDB();
		    System.out.println("Db inizializzato");
		} catch (SQLException e) {
			System.err.println("[SQLITE_ERROR] SQL error (table " + nameOfTable + " already exists)");
		} catch (ClassNotFoundException e) {
			System.err.println("ERRORE NELLA CREAZIONE");
		}
	    } else {
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:" + pathSaveDefault + File.separator + nomeDB);
		} catch (SQLException | ClassNotFoundException e) {
		    e.printStackTrace();
		}
		
	    }    
	}	
	
	public void setValueOnDB() {
		index = getListOggetti();
		String qry = "insert into " + nameOfTable + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			for (int i = 0; i < index.size(); i++) {
				cmd = con.prepareStatement(qry);
				cmd.setInt(1, i);
				cmd.setString(2, "image/null.png");
				cmd.setString(3, index.get(i).getName());
				cmd.setDouble(4, index.get(i).getVitality());
				cmd.setDouble(5, index.get(i).getStrenght());
				cmd.setDouble(6, index.get(i).getAgility());
				cmd.setDouble(7, index.get(i).getDextrerity());
				cmd.setDouble(8, index.get(i).getEndurance());
				cmd.setDouble(9, index.get(i).getIntelligence());
				cmd.setDouble(10, index.get(i).getWisdom());
				cmd.setDouble(11, index.get(i).getPerception());
				cmd.setDouble(12, index.get(i).getPhisicalDamage());
				cmd.setDouble(13, index.get(i).getMagicDamage());
				cmd.setDouble(14, index.get(i).getSpeed());
				cmd.setDouble(15, index.get(i).getStunChance());
				cmd.setDouble(16, index.get(i).getBleedingStacks());
				cmd.setDouble(17, index.get(i).getAccuracy());
				cmd.setDouble(18, index.get(i).getEvasion());
				cmd.setDouble(19, index.get(i).getCriticalStrikeChance());
				cmd.setDouble(20, index.get(i).getPhisicalDefence());
				cmd.setDouble(21, index.get(i).getMagicDefence());
				cmd.setDouble(22, index.get(i).getLuck());
				cmd.setDouble(23, index.get(i).getPureDamageEffectiveness());
				cmd.setInt(24, index.get(i).getLevel());
				cmd.setString(25, index.get(i).getRarity());
				cmd.setString(26, index.get(i).getDescription());
				cmd.setInt(27, index.get(i).getSlotInventory());
				cmd.executeUpdate();
			}
			cmd.close();
			con.close();
			index = null;
			index = new ArrayList<Item>();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createConnection(String pathSave) {
	    System.out.println("creo una connessione al db");
	    System.out.println(pathSave);
	    boolean exist = new File(pathSave).exists();
	    try {
		if (exist) {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:" + pathSave);
		    System.out.println("Connesso al db");
		}		
	    } catch (ClassNotFoundException | SQLException e) {
		System.err.println("[SQLITE_ERROR] SQL error or missing database");
	    }
	}
	
//	public Item findOggetto(String nomeOggetto) {
//	 // CONTROLLO SE L'INDEX HA GIA l?ELEMENTO
//	    try {
//		String qry = "select * from oggetto where nomeOggetto = ?";
//		cmd = con.prepareStatement(qry);    // <------------- QUA DA L'NULLPOINT
//		cmd.setString(1, nomeOggetto);
//		res = cmd.executeQuery();
//
//		Item result = null;
//		while (res.next()) {
////		    result = new Item(res.getString("percorsoImg"), res.getString("nomeOggetto"), res.getInt("prezzo"), res.getInt("qnt"));
//		}
//		if (!index.contains(result) ) {
//		    index.add(result);
//		}
//		res.close();
//		cmd.close();
//		con.close();
//		return result;
//	    } catch (SQLException e) {
//		try {
//		    cmd.close();
//		    con.close();
//	 	} catch (SQLException e1) {
//	 	    e1.printStackTrace();
//	 	}
//		e.printStackTrace();
//		return null;
//	    }
//	}
//
	public List<Item> getlistOggetti() {
		 boolean listOk = false;
		 int i = 0;

		 while (!listOk) {
			 Item result = null;
         	// richiesta al db oggetti
		try {
			String qry = "select * from " + nameOfTable + " where id=?";
			cmd = con.prepareStatement(qry);
			cmd.setInt(1, i);
			i++;
			res = cmd.executeQuery();

			while (res.next()) {
					 result = new Item(res.getInt("slotInventory"), res.getString("name"),
							 res.getInt("level"), res.getString("rarity"),
							 res.getString("description"), res.getDouble("vitality"),
							 res.getDouble("strenght"), res.getDouble("agility"),
							 res.getDouble("dextrerity"), res.getDouble("endurance"),
							 res.getDouble("intelligence"), res.getDouble("wisdom"),
							 res.getDouble("perception"), res.getDouble("phisicalDamage"),
							 res.getDouble("magicDamage"), res.getDouble("speed"),
							 res.getDouble("stunChance"), res.getDouble("bleedingStacks"),
							 res.getDouble("accuracy"), res.getDouble("evasion"),
							 res.getDouble("criticalStrikeChance"), res.getDouble("phisicalDefence"),
							 res.getDouble("magicDefence"), res.getDouble("pureDamage"),
							 res.getDouble("luck"));
					 System.out.println(result.toString());
					 if (!index.contains(result) ) {
						 index.add(result);
					 }
			}
			if (result == null) {
			listOk = true;
			}
		} catch (SQLException e) {
			try {
					 res.close();
					 cmd.close();
					 con.close();
			} catch (SQLException ex) {
			return index;
			}

			}
		}
 	    return index;
	}



	public String getNomeDB() {
	    return nomeDB;
	}
    }

}
