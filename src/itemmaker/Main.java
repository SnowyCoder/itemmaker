package itemmaker;

import itemmaker.model.Model;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("ItemMakerFXML.fxml"));
        Parent root = loader.load();
        Controller controller = loader.getController();
        Model model = new Model();
        controller.setRoot(root);
        controller.setModel(model);
        primaryStage.setTitle("ItemMaker");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.getIcons().add(new Image("itemmaker/image/anvil.png"));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
