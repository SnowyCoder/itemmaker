package itemmaker.item;

import java.util.List;

public class Item {

    private int slotInventory;
    private String name;
    private int level;
    private String rarity;
    private String description;
    private double vitality;
    private double strenght;
    private double agility;
    private double dextrerity;
    private double endurance;
    private double intelligence;
    private double wisdom;
    private double perception;
    private double phisicalDamage;
    private double magicDamage;
    private double speed;
    private double stunChance;
    private double bleedingStacks;
    private double accuracy;
    private double evasion;
    private double criticalStrikeChance;
    private double phisicalDefence;
    private double magicDefence;
    private double pureDamageEffectiveness;
    private double luck;

    public Item(int slotInventory, String name, int level, String rarity, String description, double vitality,
                double strenght, double agility, double dextrerity, double endurance, double intelligence,
                double wisdom, double perception, double phisicalDamage, double magicDamage, double speed,
                double stunChance, double bleedingStacks, double accuracy, double evasion,
                double criticalStrikeChance, double phisicalDefence, double magicDefence,
                double pureDamageEffectiveness, double luck) {


        this.slotInventory = slotInventory;
        this.name = name;
        this.level = level;
        this.rarity = rarity;
        this.description = description;
        this.vitality = vitality;
        this.strenght = strenght;
        this.agility = agility;
        this.dextrerity = dextrerity;
        this.endurance = endurance;
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.perception = perception;
        this.phisicalDamage = phisicalDamage;
        this.magicDamage = magicDamage;
        this.speed = speed;
        this.stunChance = stunChance;
        this.bleedingStacks = bleedingStacks;
        this.accuracy = accuracy;
        this.evasion = evasion;
        this.criticalStrikeChance = criticalStrikeChance;
        this.phisicalDefence = phisicalDefence;
        this.magicDefence = magicDefence;
        this.pureDamageEffectiveness = pureDamageEffectiveness;
        this.luck = luck;
    }

    public int getSlotInventory() {
        return slotInventory;
    }

    public void setSlotInventory(int slotInventory) {
        this.slotInventory = slotInventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getVitality() {
        return vitality;
    }

    public void setVitality(double vitality) {
        this.vitality = vitality;
    }

    public double getStrenght() {
        return strenght;
    }

    public void setStrenght(double strenght) {
        this.strenght = strenght;
    }

    public double getAgility() {
        return agility;
    }

    public void setAgility(double agility) {
        this.agility = agility;
    }

    public double getDextrerity() {
        return dextrerity;
    }

    public void setDextrerity(double dextrerity) {
        this.dextrerity = dextrerity;
    }

    public double getEndurance() {
        return endurance;
    }

    public void setEndurance(double endurance) {
        this.endurance = endurance;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }

    public double getWisdom() {
        return wisdom;
    }

    public void setWisdom(double wisdom) {
        this.wisdom = wisdom;
    }

    public double getPerception() {
        return perception;
    }

    public void setPerception(double perception) {
        this.perception = perception;
    }

    public double getPhisicalDamage() {
        return phisicalDamage;
    }

    public void setPhisicalDamage(double phisicalDamage) {
        this.phisicalDamage = phisicalDamage;
    }

    public double getMagicDamage() {
        return magicDamage;
    }

    public void setMagicDamage(double magicDamage) {
        this.magicDamage = magicDamage;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getStunChance() {
        return stunChance;
    }

    public void setStunChance(double stunChance) {
        this.stunChance = stunChance;
    }

    public double getBleedingStacks() {
        return bleedingStacks;
    }

    public void setBleedingStacks(double bleedingStacks) {
        this.bleedingStacks = bleedingStacks;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getEvasion() {
        return evasion;
    }

    public void setEvasion(double evasion) {
        this.evasion = evasion;
    }

    public double getCriticalStrikeChance() {
        return criticalStrikeChance;
    }

    public void setCriticalStrikeChance(double criticalStrikeChance) {
        this.criticalStrikeChance = criticalStrikeChance;
    }

    public double getPhisicalDefence() {
        return phisicalDefence;
    }

    public void setPhisicalDefence(double phisicalDefence) {
        this.phisicalDefence = phisicalDefence;
    }

    public double getMagicDefence() {
        return magicDefence;
    }

    public void setMagicDefence(double magicDefence) {
        this.magicDefence = magicDefence;
    }

    public double getPureDamageEffectiveness() {
        return pureDamageEffectiveness;
    }

    public void setPureDamageEffectiveness(double pureDamageEffectiveness) {
        this.pureDamageEffectiveness = pureDamageEffectiveness;
    }

    public double getLuck() {
        return luck;
    }

    public void setLuck(double luck) {
        this.luck = luck;
    }

    @Override
    public String toString() {
        return name + ", lvl:" + getLevel() + ", R:" + getRarity() + ", sI:" + getSlotInventory();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;

        Item item = (Item) o;

        if (getSlotInventory() != item.getSlotInventory()) return false;
        if (getLevel() != item.getLevel()) return false;
        if (Double.compare(item.getVitality(), getVitality()) != 0) return false;
        if (Double.compare(item.getStrenght(), getStrenght()) != 0) return false;
        if (Double.compare(item.getAgility(), getAgility()) != 0) return false;
        if (Double.compare(item.getDextrerity(), getDextrerity()) != 0) return false;
        if (Double.compare(item.getEndurance(), getEndurance()) != 0) return false;
        if (Double.compare(item.getIntelligence(), getIntelligence()) != 0) return false;
        if (Double.compare(item.getWisdom(), getWisdom()) != 0) return false;
        if (Double.compare(item.getPerception(), getPerception()) != 0) return false;
        if (Double.compare(item.getPhisicalDamage(), getPhisicalDamage()) != 0) return false;
        if (Double.compare(item.getMagicDamage(), getMagicDamage()) != 0) return false;
        if (Double.compare(item.getSpeed(), getSpeed()) != 0) return false;
        if (Double.compare(item.getStunChance(), getStunChance()) != 0) return false;
        if (Double.compare(item.getBleedingStacks(), getBleedingStacks()) != 0) return false;
        if (Double.compare(item.getAccuracy(), getAccuracy()) != 0) return false;
        if (Double.compare(item.getEvasion(), getEvasion()) != 0) return false;
        if (Double.compare(item.getCriticalStrikeChance(), getCriticalStrikeChance()) != 0) return false;
        if (Double.compare(item.getPhisicalDefence(), getPhisicalDefence()) != 0) return false;
        if (Double.compare(item.getMagicDefence(), getMagicDefence()) != 0) return false;
        if (Double.compare(item.getPureDamageEffectiveness(), getPureDamageEffectiveness()) != 0) return false;
        if (Double.compare(item.getLuck(), getLuck()) != 0) return false;
        if (!getName().equals(item.getName())) return false;
        if (getRarity() != null ? !getRarity().equals(item.getRarity()) : item.getRarity() != null) return false;
        return getDescription() != null ? getDescription().equals(item.getDescription()) : item.getDescription() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getSlotInventory();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getLevel();
        result = 31 * result + (getRarity() != null ? getRarity().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        temp = Double.doubleToLongBits(getVitality());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getStrenght());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getAgility());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getDextrerity());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getEndurance());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getIntelligence());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getWisdom());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getPerception());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getPhisicalDamage());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMagicDamage());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getSpeed());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getStunChance());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getBleedingStacks());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getAccuracy());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getEvasion());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getCriticalStrikeChance());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getPhisicalDefence());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMagicDefence());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getPureDamageEffectiveness());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLuck());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
